﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for VirtuelnaTastatura.xaml
    /// </summary>
    public partial class VirtuelnaTastatura : Window
    {
        public VirtuelnaTastatura()
        {
            InitializeComponent();
        }

       
        const int slova = 1;
        const int znakovi = 2;
        const int brojevi = 3;
        const int komandni = 4;
        bool shift = false;
        bool capslock = false;
        int tag;
        bool tab = false;
        public TextBox focusedtextBox { get; set; }
        public PasswordBox focusedpasswordBox { get; set; }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            tag = Int32.Parse(btn.Tag.ToString());
            if (focusedtextBox != null)
            {
                switch (tag)
                {

                    case 1:     /************************************ISPISIVANJE SLOVA*********************************************/

                        if ((shift == false && capslock == false) || (shift == true && capslock == true))
                        {
                            focusedtextBox.Text = focusedtextBox.Text + btn.Content.ToString().ToLower().ToString();
                            if (shift)
                                shift = false;
                        }
                        else if ((shift == false && capslock == true) || (shift == true && capslock == false))
                        {
                            focusedtextBox.Text = focusedtextBox.Text + btn.Content;
                            if (shift)
                                shift = false;
                        }
                        break;
                    case 2:    /****************************ISPISIVANJE SPECIJALNIH ZNAKOVA***************************************/
                        if (shift)
                        {
                            switch (btn.Content.ToString())
                            {
                                case "`":
                                    focusedtextBox.Text = focusedtextBox.Text + "~";
                                    break;
                                case "-":
                                    focusedtextBox.Text = focusedtextBox.Text + "_";
                                    break;
                                case "=":
                                    focusedtextBox.Text = focusedtextBox.Text + "+";
                                    break;
                                case "[":
                                    focusedtextBox.Text = focusedtextBox.Text + "{";
                                    break;
                                case "]":
                                    focusedtextBox.Text = focusedtextBox.Text + "}";
                                    break;
                                case ";":
                                    focusedtextBox.Text = focusedtextBox.Text + ":";
                                    break;
                                case "'":
                                    focusedtextBox.Text = focusedtextBox.Text + "\"";
                                    break;
                                case @"\":
                                    focusedtextBox.Text = focusedtextBox.Text + "|";
                                    break;
                                case ",":
                                    focusedtextBox.Text = focusedtextBox.Text + "<";
                                    break;
                                case ".":
                                    focusedtextBox.Text = focusedtextBox.Text + ">";
                                    break;
                                case "/":
                                    focusedtextBox.Text = focusedtextBox.Text + "?";
                                    break;
                            }
                            shift = false;
                        }
                        else
                        {
                            focusedtextBox.Text = focusedtextBox.Text + btn.Content;
                        }


                        break;
                    case 3:             /**********************************ISPISIVANJE BROJA***********************************************/
                        if (shift == false)
                        {
                            focusedtextBox.Text = focusedtextBox.Text + btn.Content;
                        }
                        else
                        {
                            switch (Int16.Parse(btn.Content.ToString()))
                            {
                                case 1:
                                    focusedtextBox.Text = focusedtextBox.Text + "!";
                                    break;
                                case 2:
                                    focusedtextBox.Text = focusedtextBox.Text + "@";
                                    break;
                                case 3:
                                    focusedtextBox.Text = focusedtextBox.Text + "#";
                                    break;
                                case 4:
                                    focusedtextBox.Text = focusedtextBox.Text + "$";
                                    break;
                                case 5:
                                    focusedtextBox.Text = focusedtextBox.Text + "%";
                                    break;
                                case 6:
                                    focusedtextBox.Text = focusedtextBox.Text + "^";
                                    break;
                                case 7:
                                    focusedtextBox.Text = focusedtextBox.Text + "&";
                                    break;
                                case 8:
                                    focusedtextBox.Text = focusedtextBox.Text + "*";
                                    break;
                                case 9:
                                    focusedtextBox.Text = focusedtextBox.Text + "(";
                                    break;
                                case 0:
                                    focusedtextBox.Text = focusedtextBox.Text + ")";
                                    break;
                            }
                            shift = false;
                        }
                        break;
                }
            }
            else if (focusedpasswordBox != null)
            {
                switch (tag)
                {

                    case 1:     /************************************ISPISIVANJE SLOVA*********************************************/

                        if ((shift == false && capslock == false) || (shift == true && capslock == true))
                        {
                            focusedpasswordBox.Password = focusedpasswordBox.Password + btn.Content.ToString().ToLower().ToString();
                            if (shift)
                                shift = false;
                        }
                        else if ((shift == false && capslock == true) || (shift == true && capslock == false))
                        {
                            focusedpasswordBox.Password = focusedpasswordBox.Password + btn.Content;
                            if (shift)
                                shift = false;
                        }
                        break;
                    case 2:    /****************************ISPISIVANJE SPECIJALNIH ZNAKOVA***************************************/
                        if (shift)
                        {
                            switch (btn.Content.ToString())
                            {
                                case "`":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "~";
                                    break;
                                case "-":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "_";
                                    break;
                                case "=":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "+";
                                    break;
                                case "[":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "{";
                                    break;
                                case "]":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "}";
                                    break;
                                case ";":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + ":";
                                    break;
                                case "'":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "\"";
                                    break;
                                case @"\":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "|";
                                    break;
                                case ",":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "<";
                                    break;
                                case ".":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + ">";
                                    break;
                                case "/":
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "?";
                                    break;
                            }
                            shift = false;
                        }
                        else
                        {
                            focusedpasswordBox.Password = focusedpasswordBox.Password + btn.Content;
                        }


                        break;
                    case 3:             /**********************************ISPISIVANJE BROJA***********************************************/
                        if (shift == false)
                        {
                            focusedpasswordBox.Password = focusedpasswordBox.Password + btn.Content;
                        }
                        else
                        {
                            switch (Int16.Parse(btn.Content.ToString()))
                            {
                                case 1:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "!";
                                    break;
                                case 2:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "@";
                                    break;
                                case 3:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "#";
                                    break;
                                case 4:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "$";
                                    break;
                                case 5:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "%";
                                    break;
                                case 6:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "^";
                                    break;
                                case 7:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "&";
                                    break;
                                case 8:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "*";
                                    break;
                                case 9:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + "(";
                                    break;
                                case 0:
                                    focusedpasswordBox.Password = focusedpasswordBox.Password + ")";
                                    break;
                            }
                            shift = false;
                        }
                        break;
                }
            }
            FarbanjeShifta();
        }
        private void CapsLock(object sender, RoutedEventArgs e)
        {
            capslock = !capslock;
            Button btnCaps = (Button)sender;
            if (capslock)
            {
                btnCaps.Background = Brushes.DarkGreen;
            }
            else
            {
                btnCaps.Background = new SolidColorBrush(Color.FromRgb(205, 205, 205));
            }
        }

        private void ShiftClick(object sender, RoutedEventArgs e)
        {

            Button btn = (Button)sender;
            shift = !shift;
            FarbanjeShifta();
        }
        void FarbanjeShifta()
        {
            if (shift)
            {
                shiftButton.Background = Brushes.Salmon;
                shiftBtn1.Background = Brushes.Salmon;

                foreach (Control l in gridTastature.Children)
                {
                    if (l is Label)
                        l.Background = Brushes.Salmon;
                }
            }
            else
            {
                shiftButton.Background = new SolidColorBrush(Color.FromRgb(205, 205, 205));
                shiftBtn1.Background = new SolidColorBrush(Color.FromRgb(205, 205, 205));

                foreach (Control l in gridTastature.Children)
                {
                    if (l is Label)
                        l.Background = new SolidColorBrush(Color.FromRgb(205, 205, 205));
                }
            }
        }
        private void TabClick(object sender, RoutedEventArgs e)
        {
            if (focusedtextBox != null)
            {
                focusedtextBox.Text = focusedtextBox.Text + "    ";
            }
            else if (focusedpasswordBox != null)
            {
                Button btn = (Button)sender;
                btn.IsEnabled=false;
            }
            shift = false;
            FarbanjeShifta();

        }

        private void EnterClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BackspaceClick(object sender, RoutedEventArgs e)
        {
            if (focusedtextBox != null)
            {
                if (focusedtextBox.Text.Length > 0)
                {

                    if (focusedtextBox.Text.EndsWith("    "))
                    {
                        focusedtextBox.Text = focusedtextBox.Text.Remove(focusedtextBox.Text.Length - 4);
                    }
                    else
                    {
                        focusedtextBox.Text = focusedtextBox.Text.Remove(focusedtextBox.Text.Length - 1);
                    }
                }
            }
            else if (focusedpasswordBox != null)
            { 
                if (focusedpasswordBox.Password.Length > 0)
                {
                    if (focusedpasswordBox.Password.EndsWith("    "))
                    {
                        focusedpasswordBox.Password = focusedpasswordBox.Password.Remove(focusedpasswordBox.Password.Length - 4);
                    }
                    else
                    {
                        focusedpasswordBox.Password = focusedpasswordBox.Password.Remove(focusedpasswordBox.Password.Length - 1);
                    }
                }
            }
            shift = false;
            FarbanjeShifta();
        }

        private void ClearClick(object sender, RoutedEventArgs e)
        {
            if (focusedtextBox != null)
                focusedtextBox.Text = "";
            else if (focusedpasswordBox != null)
                focusedpasswordBox.Password = "";
            shift = false;
            FarbanjeShifta();
        }

        private void SpaceClick(object sender, RoutedEventArgs e)
        {
            if (focusedtextBox != null)
            {
                focusedtextBox.Text = focusedtextBox.Text + " ";
            }
            else if (focusedpasswordBox != null)
            {
                focusedpasswordBox.Password = focusedpasswordBox.Password + " ";
            }
            shift = false;
            FarbanjeShifta();
        }
    }
}
