﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for Registracija.xaml
    /// </summary>
    public partial class Registracija : Page
    {
        public Registracija()
        {
            InitializeComponent();
        }

        public Registracija( PrijavaIRegistracija prijreg)
        {
            InitializeComponent();
            window = prijreg;
        }

        PrijavaIRegistracija window;
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            window.Frejm.Content = new Prijava();
        }

        private void FAQ_Click(object sender, RoutedEventArgs e)
        {
            window.Frejm.Content = new FAQ();
        }

        private void Ime_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        void PrikaziTastaturu(TextBox textBox, PasswordBox passwordBox)
        {
            try
            {
                VirtuelnaTastatura virtuelnaTastatura = new VirtuelnaTastatura();
                virtuelnaTastatura.focusedtextBox = textBox;
                virtuelnaTastatura.focusedpasswordBox = passwordBox;
                virtuelnaTastatura.ShowDialog();
            }
            catch { }
        }

        private void Tastatura(object sender, RoutedEventArgs e)
        {
            PrikaziTastaturu((TextBox)sender, null);

        }
        private void TastaturaLozinka(object sender, RoutedEventArgs e)
        {
            PrikaziTastaturu(null, (PasswordBox)sender);

        }
    }
}
