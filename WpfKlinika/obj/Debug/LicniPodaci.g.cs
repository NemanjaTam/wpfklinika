﻿#pragma checksum "..\..\LicniPodaci.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8C749AE7906D5A311A282A357E9F277DBA7AE13B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfKlinika;


namespace WpfKlinika {
    
    
    /// <summary>
    /// LicniPodaci
    /// </summary>
    public partial class LicniPodaci : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Ime;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Prezime;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox ImeOca;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox JMBG;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox GodRodj;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Email;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox Lozinka;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox BrTel;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Drzava;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Grad;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Adresa;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button MatKlin;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button PorLek;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Apotek;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Potvrdi;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\LicniPodaci.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Odustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfKlinika;component/licnipodaci.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\LicniPodaci.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Ime = ((System.Windows.Controls.TextBox)(target));
            
            #line 14 "..\..\LicniPodaci.xaml"
            this.Ime.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Prezime = ((System.Windows.Controls.TextBox)(target));
            
            #line 16 "..\..\LicniPodaci.xaml"
            this.Prezime.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ImeOca = ((System.Windows.Controls.TextBox)(target));
            
            #line 18 "..\..\LicniPodaci.xaml"
            this.ImeOca.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 4:
            this.JMBG = ((System.Windows.Controls.TextBox)(target));
            
            #line 20 "..\..\LicniPodaci.xaml"
            this.JMBG.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 5:
            this.GodRodj = ((System.Windows.Controls.TextBox)(target));
            
            #line 22 "..\..\LicniPodaci.xaml"
            this.GodRodj.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 6:
            this.Email = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.Lozinka = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 8:
            this.BrTel = ((System.Windows.Controls.TextBox)(target));
            
            #line 28 "..\..\LicniPodaci.xaml"
            this.BrTel.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 9:
            this.Drzava = ((System.Windows.Controls.TextBox)(target));
            
            #line 30 "..\..\LicniPodaci.xaml"
            this.Drzava.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Grad = ((System.Windows.Controls.TextBox)(target));
            
            #line 32 "..\..\LicniPodaci.xaml"
            this.Grad.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 11:
            this.Adresa = ((System.Windows.Controls.TextBox)(target));
            
            #line 34 "..\..\LicniPodaci.xaml"
            this.Adresa.GotFocus += new System.Windows.RoutedEventHandler(this.Tastatura);
            
            #line default
            #line hidden
            return;
            case 12:
            this.MatKlin = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\LicniPodaci.xaml"
            this.MatKlin.Click += new System.Windows.RoutedEventHandler(this.MatKlin_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.PorLek = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\LicniPodaci.xaml"
            this.PorLek.Click += new System.Windows.RoutedEventHandler(this.PorLek_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.Apotek = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.Potvrdi = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.Odustani = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\LicniPodaci.xaml"
            this.Odustani.Click += new System.Windows.RoutedEventHandler(this.Odustani_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

