﻿#pragma checksum "..\..\ZakazivanjePregleda.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "C4E13B8F1F3BCF68FCC68C036637631193145EC0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfKlinika;


namespace WpfKlinika {
    
    
    /// <summary>
    /// ZakazivanjePregleda
    /// </summary>
    public partial class ZakazivanjePregleda : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox Lekar;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox LekarPretraga;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox TerminPretraga;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton DatumRB;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton LekarRB;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton TerminRB;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Potvrdi;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\ZakazivanjePregleda.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Odustani;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfKlinika;component/zakazivanjepregleda.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ZakazivanjePregleda.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Lekar = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.LekarPretraga = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.TerminPretraga = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 4:
            this.DatumRB = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 5:
            this.LekarRB = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 6:
            this.TerminRB = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 7:
            this.Potvrdi = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.Odustani = ((System.Windows.Controls.Button)(target));
            
            #line 42 "..\..\ZakazivanjePregleda.xaml"
            this.Odustani.Click += new System.Windows.RoutedEventHandler(this.Odustani_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

