﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for Terapija.xaml
    /// </summary>
    public partial class Terapija : Page
    {
        public Terapija(MainWindow mainWindow)
        {
            InitializeComponent();
        }

        private void ElektrProdRec_Click(object sender, RoutedEventArgs e)
        {
            ElektronskoProduzivanjeRecepta eklektronskoProduzavanjeRecepta = new ElektronskoProduzivanjeRecepta();
            eklektronskoProduzavanjeRecepta.ShowDialog();
        }

        private void Calendar_GotFocus(object sender, RoutedEventArgs e)
        {
            TerapijaSatnica terapijaStnica = new TerapijaSatnica();
            terapijaStnica.ShowDialog();
        }
    }
}
