﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for PocetnaStranica.xaml
    /// </summary>
    public partial class PocetnaStranica : Page
    {
        public PocetnaStranica()
        {
            InitializeComponent();
        }
        public PocetnaStranica(MainWindow mainWindow)
        {
            InitializeComponent();
            
            window = mainWindow;
         

        }
        MainWindow window;

        private void ZakazivanjePregleda_Click(object sender, RoutedEventArgs e)
        {
            window.ZakazivanjePregleda.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void ZdravstveniKarton_Click(object sender, RoutedEventArgs e)
        {
            window.ZdravstveniKarton.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void ZakazaniPregledi_Click(object sender, RoutedEventArgs e)
        {
            window.ListaZakPreg.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void Terapija_Click(object sender, RoutedEventArgs e)
        {
            window.Terapija.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void IzmenaLicPod_Click(object sender, RoutedEventArgs e)
        {
            window.IzmjenaPodataka.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void PromEmILoz_Click(object sender, RoutedEventArgs e)
        {
            window.PromEmLoz.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }

        private void LogOut_Click(object sender, RoutedEventArgs e)
        {
            
            PrijavaIRegistracija prijavaIRegistracija = new PrijavaIRegistracija();
            prijavaIRegistracija.Show();
            window.Close();
        }

        private void FAQ_Click(object sender, RoutedEventArgs e)
        {
            window.Main.Content = new FAQ();
        }
    }
}
