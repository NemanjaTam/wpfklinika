﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for ZakazivanjePregleda.xaml
    /// </summary>
    public partial class ZakazivanjePregleda : Page
    {
        public ZakazivanjePregleda(MainWindow mainWindow)
        {
            InitializeComponent();
            window = mainWindow;
        }
        MainWindow window;

        private void Odustani_Click(object sender, RoutedEventArgs e)
        {
            window.PocetnaStranica.RaiseEvent(new RoutedEventArgs(ButtonBase.ClickEvent));
        }
    }
}
