﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Main.Content = new PocetnaStranica(this);
        }

        private void PocetnaStranica_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new PocetnaStranica(this);

        }

        private void ZdravstveniKarton_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new ZdravstveniKartonn(this);
        }

        private void ZakazivanjePregleda_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new ZakazivanjePregleda(this);
        }

        private void IzmjenaPodataka_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new LicniPodaci(this);
        }

        private void ListaZakPreg_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new ListaZakazanihPregleda(this);
        }

        private void Terapija_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new Terapija(this);
        }

        private void PromEmLoz_Click(object sender, RoutedEventArgs e)
        {
            Main.Content = new PromjenaEmailaILozinke(this);
        }

        public static implicit operator MainWindow(PrijavaIRegistracija v)
        {
            throw new NotImplementedException();
        }
    }
}
