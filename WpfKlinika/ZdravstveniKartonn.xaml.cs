﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for ZdravstveniKartonn.xaml
    /// </summary>
    public partial class ZdravstveniKartonn : Page
    {
        public ZdravstveniKartonn()
        {
            InitializeComponent();
        }

        public ZdravstveniKartonn(MainWindow mainWindow)
        {
            InitializeComponent();
            window = mainWindow;
        }
        MainWindow window;

        private void ListaSvihPreg_Click(object sender, RoutedEventArgs e)
        {
            window.Main.Content = new ListaPrethodnihPregleda(); 
        }

        private void Statistika_Click(object sender, RoutedEventArgs e)
        {
            window.Main.Content = new Statistika();
        }
    }
}
