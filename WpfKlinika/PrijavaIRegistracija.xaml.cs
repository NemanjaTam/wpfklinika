﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfKlinika
{
    /// <summary>
    /// Interaction logic for PrijavaIRegistracija.xaml
    /// </summary>
    public partial class PrijavaIRegistracija : Window
    {
        public PrijavaIRegistracija()
        {
            InitializeComponent();
            Frejm.Content = new Prijava(this);
            
        }
        
        private void Registracija_Click(object sender, RoutedEventArgs e)
        {
            Frejm.Content = new Registracija();
        }
    }
}
